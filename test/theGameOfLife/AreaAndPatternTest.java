package theGameOfLife;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

public class AreaAndPatternTest {


    @Test
    public void calculateCellValues() {
        AreaAndPattern area = spy(AreaAndPattern.class);
        int[][] testArray = new int[][] {{10, 10}, {10, 10}};
        doReturn(testArray).when(area).getCells();
        String expected = Arrays.deepToString(new int[][] {{13, 13}, {13, 13}});
        assertEquals(expected, Arrays.deepToString(area.CalculateCellValues().getCells()));
    }



    @Test
    public void cellLiveOrDie() {
        AreaAndPattern area = spy(AreaAndPattern.class);
        int[][] testArray = new int[][] {{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, {11, 12, 13, 14, 15, 16, 17, 18, 19, 20}};
        doReturn(testArray).when(area).getCells();
        String expected = Arrays.deepToString(new int[][] {{0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 0}, {0, 10, 10, 0, 0, 0, 0, 0, 0, 0}});
        assertEquals(expected, Arrays.deepToString(area.CellLiveOrDie().getCells()));
    }
}