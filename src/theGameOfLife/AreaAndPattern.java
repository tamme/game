package theGameOfLife;

public class AreaAndPattern {
    private int height = 30;
    private int width = 100;
    private char symbolOn = '▓';
    private char symbolOff = '.';
    private int[][] cells = new int[height][width];

    public AreaAndPattern() {
        setPattern();
    }

    private void setPattern() {
        cells[20][2] = 10;
        cells[20][3] = 10;
        cells[20][4] = 10;

        cells[0][1] = 10;
        cells[1][2] = 10;
        cells[2][0] = 10;
        cells[2][1] = 10;
        cells[2][2] = 10;

        cells[10][10] = 10;
        cells[10][11] = 10;
        cells[11][10] = 10;
        cells[11][11] = 10;
        cells[12][12] = 10;
        cells[12][13] = 10;
        cells[13][12] = 10;
        cells[13][13] = 10;


    }

    public AreaAndPattern PrintOut() {
        String singleRow = "";
        char cell;
        for (int[] row : cells) {
            for (int single : row) {
                cell = single > 0 ? symbolOn : symbolOff;
                singleRow = singleRow + cell;
                //singleRow = singleRow + String.valueOf(single);
            }
            System.out.println(singleRow);
            singleRow = "";
        }
        System.out.println("");
        return this;
    }


    public AreaAndPattern CalculateCellValues() {

        int i = 0;
        for (int[] row : getCells()) {
            int j = 0;
            for (int single : row) {
                if (single >= 10) {

                    if(j != (row.length)-1) getCells()[i][j+1]++;
                    if (j != 0) getCells()[i][j - 1]++;
                    if (i != 0) {
                        getCells()[i - 1][j]++;
                        if (j < (row.length)-1) getCells()[i - 1][j + 1]++;
                        if (j > 0) getCells()[i - 1][j - 1]++;
                    }
                    if (i < (getCells().length)-1) {
                        getCells()[i + 1][j]++;
                        if (j < (row.length)-1) getCells()[i + 1][j + 1]++;
                        if (j > 0) getCells()[i + 1][j - 1]++;
                    }
                }
                j++;
            }
            i++;
        }

        return this;
    }


    public AreaAndPattern CellLiveOrDie() {
        int i = 0;
        for (int[] row : getCells()) {
            int j = 0;
            for (int single : row) {
                if (single < 3 || (single > 3 && single < 12)  || single > 13) getCells()[i][j] = 0;
                if (single == 3 || single == 12 || single == 13) getCells()[i][j] = 10;
                j++;
            }
            i++;
        }
        return this;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }


    public int[][] getCells() {
        return cells;
    }


}
